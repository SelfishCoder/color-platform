namespace ColorPlatform.Gameplay
{
    public enum PlatformColor
    {
        All,
        Red,
        Green,
        Blue
    }
}