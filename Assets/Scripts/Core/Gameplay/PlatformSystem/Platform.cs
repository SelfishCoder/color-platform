using UnityEngine;

namespace ColorPlatform.Gameplay
{
    public class Platform : MonoBehaviour
    {
        public PlatformColor Color = default;
    }
}